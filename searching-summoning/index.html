<!doctype html>
<html lang=en>
  <head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <link rel=icon type=image/svg+xml href=/favicon.svg>
    <link rel=me href="https://social.treehouse.systems/@mkhl">
    <link rel=stylesheet href=/default.css>
    <link rel=alternate type=application/atom+xml title=Atom href=/atom.xml>

    <title>Searching and Summoning</title>
  </head>
  <body>
    <main>
      <h1>Searching and Summoning</h1>
      
      <p>Computers these days usually have some way to let humans search for stuff.
Most of them present the results in a long list for the human to navigate.
But depending on what you're trying to do, that might not be the best or even a reasonable interface to use.</p>
<h2 id="what-is-searching-and-summoning">What is Searching and Summoning?</h2>
<p>Let's think about movies for a bit.
Imagine you're looking for movies starring Jet Li.
You might use some computer interface, type some terms like &quot;Jet Li&quot;, and it might present a list of movies he starred in.
This is <em>searching</em>, and a list of search results is a completely reasonable result to get here, you might care about any of them.</p>
<p>Now imagine you're instead looking for the <em>specific</em> movie called Hero (from 2002 and starring Jet Li).
This is a different kind of activity I call <em>summoning</em>:
you know what you want and you want it to be <em>here</em>, <em>now</em>.
You don't care about other movies with Jet Li, or other movies called Hero.
Being presented a list containing all those movies is now an <em>obstacle</em> that the computer is putting in your way.</p>
<p>Of course the computer cannot <em>know</em> what we want to summon so something like a list seems like a necessity,
but having to find the thing we want from that list means that we are <em>lacking affordances to be specific enough about what we want</em>.</p>
<h2 id="when-is-summoning-relevant">When is Summoning Relevant?</h2>
<p>Consider these screenshots I took on my machine.
They show search results for the term &quot;sound&quot; in the GNOME Shell Overview.</p>
<p><img src="https://mkhl.codeberg.page/searching-summoning/screenshot-sound-1.png" alt="screenshot of the first page of GNOME overview search results for the term &quot;sound&quot;, you see applications, settings, and music albums; one of the results from settings is called &quot;sound&quot;" title="GNOME Overview search results for the term sound, page 1" />
<img src="https://mkhl.codeberg.page/searching-summoning/screenshot-sound-2.png" alt="screenshot of the second page of GNOME overview search results for the term &quot;sound&quot;, you see files and emoji; one of the results from emoji is called &quot;sound&quot;" title="GNOME Overview search results for the term sound, page 2" /></p>
<p>In case you're unfamiliar, GNOME supports applications being &quot;search providers&quot; which lets them contribute sections of results to this overview search.
In the system settings you can enable or disable these search providers, and define the order their sections appear in.
So you have to specify the order <em>once</em> for <em>all</em> future searches.</p>
<p>Which result was I looking for with this search? It depends!
In the past I probably would have wanted the Sound Settings.
For a while, when I searched for &quot;ound&quot;, the only result I would get would be the Sound Settings, because application search didn't support that kind of matching.
This was a happy coincidence – I'd discovered that &quot;ound&quot; took me immediately to what I was thinking of, and it was because of the way GNOME search was working.
An accident of the design had become useful infrastructure for me.</p>
<p>So looking at the above screenshots again,  when I type &quot;sound&quot; it's most likely that I'm looking for the Sound Settings.
There are better ways of finding the applications and soundtracks we can see in the screenshots but if I try to drill down – by adding more search terms – then the Sound Settings won't appear in the results anymore.
But I might also want the emoji, the other result that'd disappear if I tried to drill down.
And it's so far down the lists it doesn't even fit on my screen!</p>
<p>This would not be a problem if I could indicate in which <em>context</em> I want to search.
Staying with the same example, I could specify this context by choosing settings, or emoji, or even music, or by staying with the default (applications). 
The resulting list would be much <em>shorter</em>.
And while that might be a shortcoming while searching, it's an enormous boon while summoning!
Sadly there is no way for me to tell the computer which kind of thing I'm looking for so I'm left to navigate this list.</p>
<h2 id="where-did-summoning-come-from">Where did Summoning Come From?</h2>
<p>The searching/summoning terminology comes from <a href="https://www.youtube.com/watch?v=P_WOPkT-9EI">Nicholas Jitkoff's excellent Google Tech Talk about Quicksilver</a>, and it resonated with me.
Nicholas created <a href="https://qsapp.com/">Quicksilver</a>, and later (after he joined Google) created a kind of spiritual successor in <a href="https://code.google.com/archive/p/qsb-mac/">Quick Search Box</a> that inspired and informed much of my thinking around this.</p>
<p>Both of these applications were all about context.
We'll come back to how you use them in a bit.</p>
<h2 id="why-command-lines-are-not-enough">Why Command Lines are Not Enough</h2>
<p>Launchers that became popular later, like <a href="https://www.alfredapp.com/">Alfred</a> or open-source clones like <a href="https://ulauncher.io/">Ulauncher</a> or <a href="https://albertlauncher.github.io/">Albert</a>, let you define <em>custom prefixes</em> for your searches that essentially define their context.
But this turns the search interface into a <em>command line</em> with all the downsides of a command line interface, including incoherence and lack of discoverability.</p>
<p>Imagine instead if, using an application like this that's ostensibly all about searching for stuff, you could <em>search</em> for the <em>context</em> you want to use instead of memorizing its prefix.
Imagine if there were no artificial distinction between your password manager as an application, and your password manager as a search context.</p>
<p>That's the approach taken by Quicksilver and QSB:
you find things and then decide how to use them;
open or launch them, browse them, search inside them… whatever you choose.
And that would be an interface I'd <em>enjoy</em> using.</p>
<h2 id="introducing-gnome-summoner">Introducing: Gnome Summoner</h2>
<p>So I decided to bring this together in an application that I can use on my computer.
I call it Gnome Summoner.
Here it is in action, letting me search for terms and contexts - <em>summoning</em>:</p>
<iframe title="Gnome Summoner Demo Screencast" width="560" height="315" src="https://tube.tchncs.de/videos/embed/ade0bc4e-f3fd-4d16-943d-75ca433a9705" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
<p>Update: <a href="https://codeberg.org/mkhl/gnome-summoner/src/commit/457c0ca78fce4ebbad2b12d46efef2cb5f499baf">And it's available now!</a></p>

    </main>
  </body>
</html>
