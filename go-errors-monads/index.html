<!doctype html>
<html lang=en>
  <head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <link rel=icon type=image/svg+xml href=/favicon.svg>
    <link rel=me href="https://social.treehouse.systems/@mkhl">
    <link rel=stylesheet href=/default.css>
    <link rel=alternate type=application/atom+xml title=Atom href=/atom.xml>

    <title>Error Handling In Go (With Monads)</title>
  </head>
  <body>
    <main>
      <h1>Error Handling In Go (With Monads)</h1>
      
      <aside>
        Originally published at
        <a href="https:&#x2F;&#x2F;www.innoq.com&#x2F;en&#x2F;blog&#x2F;golang-errors-monads&#x2F;">the INNOQ blog</a>
      </aside>
      
      <h2 id="errors-are-values">Errors are values</h2>
<p>In his post <a href="https://blog.golang.org/errors-are-values">&quot;Errors are values&quot;</a>,
Rob Pike, one of the original authors of Go,
attends the common perception
that one must repetitively type</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">if err != nil
</code></pre>
<p>in order to handle errors.</p>
<p>He recounts an encounter of his with another Go programmer
who had some code that looked like this
(omitting the slice expressions
because they’re irrelevant here):</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">_, err = fd.Write(p0)
if err != nil {
    return err
}
_, err = fd.Write(p1)
if err != nil {
    return err
}
_, err = fd.Write(p2)
if err != nil {
    return err
}
&#x2F;&#x2F; and so on
</code></pre>
<p>To help solve the repetition,
Rob defined a type called <code>errWriter</code></p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">type errWriter struct {
	w   io.Writer
	err error
}
</code></pre>
<p>with a <code>write</code> method
that stops writing to <code>w</code> as soon as it hits the first error:</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">func (ew *errWriter) write(buf []byte) {
    if ew.err != nil {
        return
    }
    _, ew.err = ew.w.Write(buf)
}
</code></pre>
<p>This encapsulates the repetitive error handling
and lets them simplify the above code to something like this:</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">ew := &amp;errWriter{w: fd}
ew.write(p0)
ew.write(p1)
ew.write(p2)
&#x2F;&#x2F; and so on
if ew.err != nil {
    return ew.err
}
</code></pre>
<p>He then notes that this pattern appears often
in the Go standard library,
including in the <a href="https://golang.org/pkg/bufio/#Writer"><code>bufio.Writer</code></a> class
which provides the same error handling as <code>errWriter</code> above
while satisfying the <a href="http://golang.org/pkg/io/#Writer"><code>io.Writer</code></a> interface.</p>
<p>Using it changes the example into this:</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">b := bufio.NewWriter(fd)
b.Write(p0)
b.Write(p1)
b.Write(p2)
&#x2F;&#x2F; and so on
if b.Flush() != nil {
    return b.Flush()
}
</code></pre>
<p>Rob closes by stating:</p>
<blockquote>
<p>Use the language to simplify your error handling.</p>
</blockquote>
<h2 id="using-the-language">Using the language</h2>
<p>The above solution is specific to <a href="http://golang.org/pkg/io/#Writer"><code>io.Writer</code></a>,
even though the same error handling strategy
makes sense for the <a href="http://golang.org/pkg/io/#Reader"><code>io.Reader</code></a> type,
and from the blog post we know that it is in fact repeated
in <a href="https://golang.org/pkg/bufio/#Scanner"><code>bufio.Scanner</code></a>
and the <a href="http://golang.org/pkg/archive/zip/"><code>archive/zip</code></a>
and <a href="http://golang.org/pkg/net/http/"><code>net/http</code></a> packages.</p>
<p>Go does not support <a href="https://en.wikipedia.org/wiki/Parametric_polymorphism">parametric polymorphism</a>
(or &quot;Generics&quot;),
but if it did we could use it to write
a single implementation of this error handling pattern
and reuse it for different types.</p>
<p>Let’s check out what that might look like.</p>
<h2 id="result">Result</h2>
<p>Let’s start by considering <a href="http://golang.org/pkg/io/#Writer"><code>io.Writer</code></a>.
It is an interface with exactly one method:</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">type Writer interface {
	Write(p []byte) (n int, err error)
}
</code></pre>
<p>That method’s return type is a pair consisting of a value and an error,
where in the common case that error is <code>nil</code>,
indicating that the operation finished successfully.
If an error did occur, the value may still be present (and non-zero),
but we’ll ignore that case for this blog post.</p>
<p>With a sufficiently expressive type system
(and using <em>completely</em> made up syntax)
we could express this as a type <code>Result&lt;A&gt;</code>, say,
which represents the result of a computation
and covers two cases:</p>
<ul>
<li>we either have some value of type <code>A</code> if the computation was successful</li>
<li>or we have some error (of type <code>error</code>) if the computation failed</li>
</ul>
<p>An implementation could look similar to this:</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">type Result&lt;A&gt; struct {
	&#x2F;&#x2F; fields
}

func (r Result&lt;A&gt;) Value() A {…}
func (r Result&lt;A&gt;) Error() error {…}
</code></pre>
<p>This type provides a place to put the error handling strategy we’re after:
From a successful <code>Result</code> we want to run the next “step” of our program,
which may itself return a <code>Result</code>,
but as soon as one step fails we want to stop.
Let’s define a method <code>Then</code> for this task:</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">func (r Result&lt;A&gt;) Then(f func() Result&lt;A&gt;) Result&lt;A&gt; {…}
</code></pre>
<p>If <code>r</code> contains an error, <code>Then</code> just returns <code>r</code>,
otherwise it calls <code>f</code> and returns the result of that call,
which is exactly what we wanted.
So far so good.</p>
<h2 id="polishing">Polishing</h2>
<p>You may have noticed that calling <code>Then</code>
simply discards the value of <code>r</code>
(if it’s a <em>successful</em> <code>Result</code>).
We also don’t need <code>Then</code> to always return
a <code>Result</code> containing the <em>same</em> type of value.
Let’s lift those restrictions and generalize the method:</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">func (r Result&lt;A&gt;) Then(f func(A) Result&lt;B&gt;) Result&lt;B&gt; {…}
</code></pre>
<p>Note that <code>f</code> is still free to ignore its argument
or to return a <code>Result</code> containing a value of type <code>A</code>,
and that a <code>Result</code> containing an error
satisfies <code>Result&lt;A&gt;</code> for <em>any</em> type <code>A</code>.</p>
<p>Using this type (and a <code>Write</code> method that returns it),
the example code from above could look something like this:</p>
<pre data-lang="go" class="language-go "><code class="language-go" data-lang="go">r := fd.Write(p0).Then(func(_) {
		return fd.Write(p1)
	}).Then(func(_) {
		return fd.Write(p2)
	})
&#x2F;&#x2F; and so on
if r.Error() != nil {
	return r.Error()
}
</code></pre>
<p>I’ll be the first to admit that this piece of code is not elegant,
but I believe that that’s due to lack of support by the language,
so let’s consider where improving that could get us.</p>
<h2 id="the-m-word">The M-Word</h2>
<p>The method <code>Then</code> we defined above is well known
in certain circles that practice functional programming,
only those folks usually call it <code>flatMap</code> (or <code>bind</code>).
That’s because the <code>Result</code> type is a <em>monad</em>.
(Some other names for similar types are
<a href="https://github.com/antitypical/Result"><code>Result</code></a>,
<a href="http://hackage.haskell.org/package/base-4.9.0.0/docs/Data-Either.html#t:Either" title="Data.Either"><code>Either</code></a>,
<a href="http://doc.scalatest.org/2.2.6/#org.scalactic.Or" title="ScalaTest 2.2.6"><code>Or</code></a>,
<a href="http://typelevel.org/cats/tut/xor.html" title="Cats Documentation - Xor"><code>Xor</code></a>, or
<a href="https://oss.sonatype.org/service/local/repositories/releases/archive/org/scalaz/scalaz_2.11/7.2.3/scalaz_2.11-7.2.3-javadoc.jar/!/index.html#scalaz.$bslash$div"><code>\/</code></a>.)</p>
<p>And in languages with better support for monads
we can more easily express computations using them.
This is what the same piece of code looks like in Haskell:</p>
<pre data-lang="haskell" class="language-haskell "><code class="language-haskell" data-lang="haskell">do	write fd p0
	write fd p1
	write fd p2
</code></pre>
<p>Yes, this code performs the exact same error handling as our examples above.
Other code that handles errors the same way will also look the same,
reusing the error handling strategy defined in the result type,
removing the need to wrap facades around interfaces
every time we want to handle the errors they generate.
(You may think that this fixes
the error handling strategy of each function
by way of its return type,
but there are ways to parameterize those as well.)</p>
<p>In the end we accomplished exactly what is being preached for Go:
We treat errors as values,
and we are using our language to simplify our error handling.
The difference is that we only have to do that <em>once</em>.</p>
<h2 id="conclusion">Conclusion</h2>
<p>Two common perceived problems of Go are
that handling errors is verbose and repetitive
and that parametric polymorphism is unavailable.
(<a href="http://research.swtch.com/generic">Russ Cox provides a reasonable explanation why.</a>)</p>
<p>One of the authors of Go offers a solution to one of those problems,
but his advice boils down to “use monads,”
and because of the other problem
you <em>cannot</em> express this concept in Go.</p>
<p>This leaves us having to implement artisanal one-off monads
for every interface we want to handle errors for,
which I think is still as verbose and repetitive.</p>

    </main>
  </body>
</html>
