<!doctype html>
<html lang=en>
  <head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <link rel=icon type=image/svg+xml href=/favicon.svg>
    <link rel=me href="https://social.treehouse.systems/@mkhl">
    <link rel=stylesheet href=/default.css>
    <link rel=alternate type=application/atom+xml title=Atom href=/atom.xml>

    <title>When You Have Reached Acme</title>
  </head>
  <body>
    <main>
      <h1>When You Have Reached Acme</h1>
      
      <aside>
        Originally published at
        <a href="https:&#x2F;&#x2F;www.innoq.com&#x2F;en&#x2F;blog&#x2F;mkhl-acme-setup&#x2F;">the INNOQ blog</a>
      </aside>
      
      <p>My main text editing environment these days is <a href="http://acme.cat-v.org/">Acme</a>,
and has been for a while now.</p>
<p>If you're unfamiliar with Acme,
<a href="https://research.swtch.com/acme">Russ Cox has recorded an excellent demonstration</a>.
This is not an introduction to Acme,
nor is it a setup guide.
But if you've tried or seen or read about Acme
and were wondering how people could <em>possibly</em> be productive with it,
this might help point you in some direction.
Or it might not, but it is how <em>I</em> use it these days.</p>
<p>Since my main computer doesn't run <a href="https://9p.io/plan9/">Plan 9</a>,
I use Acme from <a href="https://9fans.github.io/plan9port/">Plan 9 from User Space</a>,
a port of large portions of the Plan 9 user space to Unix,
with a handful of customization.
You can find some of these customizations
<a href="https://github.com/mkhl/plan9port/branches">in the branches of my fork</a>,
and a merged version of the ones I currently use
<a href="https://github.com/mkhl/plan9port/tree/current">in my &quot;current&quot; branch</a>.</p>
<h2 id="getting-started">Getting Started</h2>
<p>When working on a project I usually start by opening its guide file,
which is a collection of shell and Acme commands that are useful for this project.
The <a href="http://doc.cat-v.org/plan_9/4th_edition/papers/acme/">original Acme paper</a> contains a few examples.
From there I use xplor, <code>t</code> and <code>g</code> to navigate.</p>
<h2 id="search">Search</h2>
<p><code>T</code> is a fuzzy finder;
it generates a list of files
and filters that using some fuzzy search
(currently <a href="https://github.com/junegunn/fzf">fzf</a>).
Where in other environments one would use <kbd>⌘T</kbd> to open a dialog,
enter &quot;sometexttofilterwith&quot;,
then select and open the results,
I 2 (middle-click) &quot;t sometexttofilterwith&quot;,
then 3 any of the results to open them.</p>
<p><code>G</code> is a content search tool like grep
(currently <a href="https://github.com/BurntSushi/ripgrep">ripgrep</a>),
configured to search recursively.
It replaces the &quot;Find in Project&quot;-style search
found in other environments.</p>
<p>The most straightforward way to navigate using external programs is
by making sure they write positions in the traditional format
<code>$filename:$line</code> or <code>$filename:$line:$column</code>.
This lets Acme users 3 (right-click) on a position
to open that file and select that line or column,
essentially turning all of those positions into hyperlinks.
Of course, I configured both <code>t</code> and <code>g</code> to do this.</p>
<h2 id="navigation">Navigation</h2>
<p>Acme exposes its state as a filesystem.
In this filesystem there is a directory corresponding to each open window,
and external programs can use the files in this directory
to manipulate things like the window contents or selection.
It also contains a log file that emits events generated in that window by the user.</p>
<p>When an external program opens this log,
Acme assumes that it will take over handling any interaction in the window.
Programs that do this are called &quot;external client programs&quot; in the documentation,
and <a href="https://github.com/mkhl/xplor">xplor</a> is an example of one.</p>
<p>Xplor is a tree-style file explorer for Acme
that was originally written by <a href="https://github.com/mpl">Mathieu Lonjaret</a>
and that I massively rewrote.
Taking over event handling for the window lets us override 3 on directories, say,
to toggle whether xplor displays the contents of that directory.</p>
<p>This makes xplor useful for drilling down into familiar
or exploring unfamiliar directories,
and given the deep directory hierarchies that are common these days
it feels less cumbersome to use than Acme's native directory editing,
which opens every subdirectory in its own window.</p>
<h2 id="modification">Modification</h2>
<p>External programs don't need to handle events though,
as both <a href="https://godoc.org/github.com/mkhl/cmd/acme/acmepipe">acmepipe</a> and <a href="https://godoc.org/github.com/mkhl/cmd/acme/acmeeval">acmeeval</a> demonstrate.</p>
<p>Acmepipe filters the window contents
through some command.
It works like <code>|</code> inside Acme,
but it doesn't clear the window contents when the command fails,
it can apply the changes in separate chunks,
and we can call it from outside of Acme.</p>
<p>Acmeeval programmatically runs commands
in Acme's interactive language,
possibly from outside of Acme.
It works by adding the commands to the tag
(the &quot;menu&quot; above each window)
and generating an execution event for their region in the tag.</p>
<h2 id="automation">Automation</h2>
<p>When I start Acme, I also run</p>
<pre><code>autoacme ~&#x2F;bin&#x2F;acme-editorconfig
autoacme ~&#x2F;bin&#x2F;acme-autoformat
</code></pre>
<p>(except in the background)
so Acme respects editorconfig settings
and automatically runs formatters and linters.</p>
<p><a href="https://godoc.org/github.com/mkhl/cmd/acme/autoacme">Autoacme</a> is similar to an external client program,
but instead of a window log it reads events from the global Acme log
and runs a command for each one it receives.</p>
<p><a href="https://gist.github.com/mkhl/5e4cda4f9a262f432eacd592aba5fd54">Acme-editorconfig</a>
handles <code>new</code> events,
which happen when Acme opens a new window.
It runs <code>editorconfig</code> on the window's file,
translates the output into Acme commands,
and uses acmeeval to execute them.
This requires the &quot;soft-tabs&quot; patch described below.</p>
<p><a href="https://gist.github.com/mkhl/69e2be41bfeccb368b52818ebd7f535b">Acme-autoformat</a>
handles <code>put</code> events,
which Acme generating when saving files.
It determines the type of the saved file,
runs the configured formatter on it,
and updates the window content with the result using acmepipe.
For types without a fitting formatter,
it runs a linter instead.</p>
<p>Ideally I'd like to run both a linter and a formatter,
but I'm not sure how to make that work:
The formatter updates the window contents,
so running it after the linter means
that the line numbers in the lint report could be off,
but running it before the linter means
the linter would have to read if from stdin
so its report would be missing the file name.</p>
<h2 id="soft-tabs">Soft Tabs</h2>
<p>The <a href="https://github.com/mkhl/plan9port/tree/acme/soft-tabs">acme/soft-tabs</a> branch adds &quot;soft tabs&quot; support to Acme.</p>
<p>When this mode is active,
Acme emulates soft tabs by having
the <kbd>⇥</kbd> key insert <code>$tabwidth</code> space characters,
and
the <kbd>⌫</kbd> key delete as many space characters.
Without it, Acme only ever inserts single tab or space characters.</p>
<p>The change originates in <a href="http://9front.org/">9front</a>, a modern fork of Plan 9,
and was ported by <a href="https://github.com/neutralinsomniac">neutralinsomniac</a>.
It has been rejected upstream
but is very useful when working on codebases
where the authors expect indentation using spaces.</p>
<h2 id="conclusion">Conclusion</h2>
<p>There are still more tools for working with Acme,
some of which I haven't had a chance to use yet,
including <a href="https://github.com/fhs/acme-lsp">a language server protocol implementation</a>!</p>
<p>If you are excited about trying Acme,
or if you are already using it,
or if you know other Acme-related tools I should try,
or if I got anything wrong,
or for any other reason at all,
please don't hesitate to get in touch!</p>

    </main>
  </body>
</html>
